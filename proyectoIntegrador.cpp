#include <iostream>

using namespace std;

string mayuscula(string cadena){
	for (int i = 0; i < cadena.length(); i++) {
    	cadena[i] = toupper(cadena[i]);
  	}
  	return cadena;
}

struct NodoEstProf{
	int codigo;
	string nombre;
	string correo;
	NodoEstProf* siguiente;
};

struct Materia{
	string codigo;
	string nombre;
	Materia* siguiente;
};

void insertarEstProf(NodoEstProf* &cabecera, int codigo, string nombre, string correo){
	NodoEstProf* nuevo = new NodoEstProf;
	nuevo->codigo = codigo;
	nuevo->nombre = nombre;
	nuevo->correo = correo;
	nuevo->siguiente = NULL;
	if(cabecera == NULL){
		cabecera = nuevo; 
	}else{
		NodoEstProf* ultimo = cabecera;
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}
		ultimo->siguiente = nuevo;
	}
}

void imprimirListaEstProf(NodoEstProf* cabecera){
	int i = 1;
	NodoEstProf* aux = cabecera;
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"|#|\t|Codigo|\t|Nombre|\t\t\t|Correo|\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	while(aux != NULL){
		cout<< "|" << i << ".|\t|" << aux->codigo << "|\t|" << aux->nombre << "|\t\t\t|" << aux->correo << "|\n";
		cout<<"-----------------------------------------------------------------------------------------------------\n";
		i++;
		aux = aux->siguiente;
	}
	
}

NodoEstProf* buscarEstProfCodigo(NodoEstProf* &cabecera, int codigoBuscar){
	if(cabecera == NULL){
		return NULL;
	}
	NodoEstProf* aux = cabecera;
	while(aux->codigo != codigoBuscar){
		aux = aux->siguiente;
		if(aux == NULL){
			return NULL;
		}	
	}
		
	return aux;
}

void modificarEstProf(NodoEstProf* &estProfModificar, string nombre, string correo){
	estProfModificar->nombre = nombre;
	estProfModificar->correo = correo;
}

void eliminarEstProf(NodoEstProf* &cabecera, NodoEstProf* &eliminar){
	NodoEstProf* aux = cabecera;
	if(eliminar == cabecera){
		cabecera = cabecera->siguiente;
	}else{
		while(aux->siguiente != eliminar){
			aux = aux->siguiente;
		}
		aux->siguiente = eliminar->siguiente;
	}
	delete eliminar;
}

void leer(string &variable){
	cin.ignore();
	getline(cin, variable);
}

void menuEstProf(NodoEstProf* &cabecera, string plural, string singular){
	int menu = 0;
	int codigo;
	string nombre;
	string correo;
	NodoEstProf* buscarEstProf = NULL;
	while(menu != 6){
		cout<<"\t\t\tMENU "<< mayuscula(plural) << "\n";
		cout<<"1. Registrar "<< singular << ".\n";
		cout<<"2. Imprimir lista de " << plural << ".\n";
		cout<<"3. Buscar " << singular << " por codigo.\n";
		cout<<"4. Modificar informacion de un " << singular << ".\n";
		cout<<"5. Eliminar un " << singular << ".\n";
		cout<<"6. Salir.\n";
		cin>>menu;
		switch(menu){
			case 1:
				do{
					cout<<"Escriba el codigo del " << singular << ": ";
					cin>>codigo;
					buscarEstProf = buscarEstProfCodigo(cabecera, codigo);
					if(buscarEstProf != NULL){
						cout<<"Ese codigo ya esta registrado.\n";
					}
				}while(buscarEstProf != NULL);
				cout<<"Escriba el nombre del " << singular << ": ";
				leer(nombre);
				cout<<"Escriba el correo del " << singular << ": ";
				cin>>correo;
				insertarEstProf(cabecera, codigo,nombre, correo);
			break;
			case 2: 
				imprimirListaEstProf(cabecera);
			break;
			case 3:
				if(cabecera == NULL){
					cout<<"No hay "<< plural << " registrados.\n";
				}else{
					do{
						cout<<"Escriba el codigo del " << singular << " que quiere buscar: ";
						cin>>codigo;
						buscarEstProf = buscarEstProfCodigo(cabecera, codigo);
						if(buscarEstProf == NULL){
							cout<<"No hay ningun " << singular << " registado con ese codigo.\n";
						}else{
							cout<<"-----------------------------------------------------------------------------------------------------\n";
							cout<<"|#|\t|Codigo|\t|Nombre|\t\t\t|Correo|\n";
							cout<<"-----------------------------------------------------------------------------------------------------\n";
							cout<< "|1.|\t|" << buscarEstProf->codigo << "|\t|" << buscarEstProf->nombre << "|\t\t\t|" << buscarEstProf->correo << "|\n";
							cout<<"-----------------------------------------------------------------------------------------------------\n";	
						}
					}while(buscarEstProf == NULL);
				}
			break;	
			case 4:
				if(cabecera == NULL){
					cout<<"No hay " << plural << " registrados.\n";
				}else{
					imprimirListaEstProf(cabecera);
					do{
						cout<<"Escriba el codigo del " << singular << " que quiere modificar: ";
						cin>>codigo;
						buscarEstProf = buscarEstProfCodigo(cabecera, codigo);
						if(buscarEstProf == NULL){
							cout<<"No hay ningun " << singular << " registado con ese codigo.\n";
						}else{
							cout<<"Escriba el nombre del " << singular << ": ";
							leer(nombre);
							cout<<"Escriba el correo del " << singular << ": ";
							cin>>	correo;
							modificarEstProf(buscarEstProf, nombre, correo);
						}
					}while(buscarEstProf == NULL);
				}
			break;
			case 5:
				if(cabecera == NULL){
					cout<<"No hay " << plural << " registrados.\n";
				}else{
					imprimirListaEstProf(cabecera);
					do{
						cout<<"Escriba el codigo del " << singular << " que quiere eliminar: ";
						cin>>codigo;
						buscarEstProf = buscarEstProfCodigo(cabecera, codigo);
						if(buscarEstProf == NULL){
							cout<<"No hay ningun " << singular <<" registado con ese codigo.\n";
						}else{
							eliminarEstProf(cabecera,buscarEstProf);
						}
					}while(buscarEstProf == NULL);
				}
			break;
			case 6:
				cout<<"salida\n";
			break;
		}
	}
}

struct NodoMateria{
	string codigo;
	string nombre;
	NodoMateria* sigueinte;
};

void registrarMateria(NodoMateria* &cabecera, string codigo, string nombre){
	NodoMateria* nuevo = new NodoMateria;
	nuevo->codigo = codigo;
	nuevo->nombre = nombre;
	nuevo->sigueinte = NULL;
	if(cabecera == NULL){
		cabecera = nuevo;
	}else{
		NodoMateria* ultimo = cabecera;
		while(ultimo->sigueinte != NULL){
			ultimo = ultimo->sigueinte;
		}
		ultimo->sigueinte = nuevo;
	}
}

void imprimirMaterias(NodoMateria* cabecera){
	NodoMateria* aux = cabecera;
	int i = 1;
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"|#|\t|Codigo|\t|Nombre|\t\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	while(aux != NULL){
		cout<< "|" << i << ".|\t|" << aux->codigo << "|\t\t|" << aux->nombre << "|" << endl;
		cout<<"-----------------------------------------------------------------------------------------------------\n";
		aux = aux->sigueinte;
		i++;
	}
	cout<<endl;
}

NodoMateria* buscarMateriaCodigo(NodoMateria* cabecera, string buscar){
	if(cabecera == NULL){
		return NULL;
	}else{
		NodoMateria* aux = cabecera;
		while(aux != NULL){
			if(aux->codigo == buscar){
				return aux;
			}
			aux = aux->sigueinte;
		}
	}
	return NULL;	
}

void modificarMateria(NodoMateria* &modificar, string nombre){
	modificar->nombre = nombre;
}

void eliminarMateria(NodoMateria* &cabecera, NodoMateria* &eliminar){
	if(cabecera == eliminar){
		cabecera = cabecera->sigueinte;
	}else{
		NodoMateria* aux = cabecera;
		while(aux->sigueinte != eliminar){
			aux = aux->sigueinte;
		}
		aux->sigueinte = eliminar->sigueinte;
	}
	delete eliminar;
}


void menuMaterias(NodoMateria* &cabecera){
	int menu = 0;
	string codigo;
	string nombre;
	NodoMateria* buscarMateria = NULL;
	while(menu != 6){
		cout<<"\t\t\tMENU MATERIAS\n";
		cout<<"1. Registrar materia.\n";
		cout<<"2. Imprimir lista de materias.\n";
		cout<<"3. Buscar materia por codigo.\n";
		cout<<"4. Modificar informacion de una materia.\n";
		cout<<"5. Eliminar una materia.\n";
		cout<<"6. Salir.\n";
		cin>>menu;
		
		switch(menu){
			case 1:
				do{
					cout<<"Escriba el codigo de la materia: ";
					cin>>codigo;
					buscarMateria = buscarMateriaCodigo(cabecera, codigo);
					if(buscarMateria != NULL){
						cout<<"Ese codigo ya esta registrado.\n";
					}
				}while(buscarMateria != NULL);
				cout<<"Escriba el nombre de la materia: ";
				leer(nombre);
				registrarMateria(cabecera, codigo, nombre);
			break;
			case 2:
				imprimirMaterias(cabecera);
			break;
			case 3:
				do{
					cout<<"Escriba el codigo de la materia que quiere buscar: ";
					cin>>codigo;
					buscarMateria = buscarMateriaCodigo(cabecera, codigo);
					if(buscarMateria == NULL){
						cout<<"No hay una materia registrada con ese codigo.\n";
					}
				}while(buscarMateria == NULL);
				cout<<"-----------------------------------------------------------------------------------------------------\n";
				cout<<"|#|\t|Codigo|\t|Nombre|\t\n";
				cout<<"-----------------------------------------------------------------------------------------------------\n";
				cout<< "|1.|\t|" << buscarMateria->codigo << "|\t\t|" << buscarMateria->nombre << "|" << endl;
				cout<<"-----------------------------------------------------------------------------------------------------\n";
			break;
			case 4:
				do{
					imprimirMaterias(cabecera);
					cout<<"Escriba el codigo de la materia que quiere modificar: ";
					cin>>codigo;
					buscarMateria = buscarMateriaCodigo(cabecera, codigo);
					if(buscarMateria == NULL){
						cout<<"No hay una materia registrada con ese codigo.\n";
					}
				}while(buscarMateria == NULL);
				cout<<"Escriba el nuevo nombre de la materia: ";
				leer(nombre);
				modificarMateria(buscarMateria, nombre);
			break;
			case 5:
				do{
					imprimirMaterias(cabecera);
					cout<<"Escriba el codigo de la materia que quiere eliminar: ";
					cin>>codigo;
					buscarMateria = buscarMateriaCodigo(cabecera, codigo);
					if(buscarMateria == NULL){
						cout<<"No hay una materia registrada con ese codigo.\n";
					}
				}while(buscarMateria == NULL);
				eliminarMateria(cabecera, buscarMateria);
			break;
			case 6:
			cout<<"salida\n";
			break;
		}	
	}
}

struct NodoEstudianteGrupo{
	NodoEstProf* estudiante;
	NodoEstudianteGrupo* siguiente;
};

struct NodoGrupo{
	int codigo;
	NodoMateria* materia;
	NodoEstProf* profesor;
	NodoEstudianteGrupo* cabeceraEstudiantes;
	NodoGrupo* siguiente;
};

int getCodigoGrupo(NodoGrupo* cabecera){
	NodoGrupo* ultimo = cabecera;
	if(cabecera == NULL){
		return 1; 
	}else{
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}	
	}
	return (ultimo->codigo)+1;
}

void registrarGrupo(NodoGrupo* &cabecera, NodoMateria* materia, NodoEstProf* profesor){
	NodoGrupo* nuevo = new NodoGrupo;
	nuevo->codigo = getCodigoGrupo(cabecera);
	nuevo->materia = materia;
	nuevo->profesor = profesor;
	nuevo->cabeceraEstudiantes = NULL;
	nuevo->siguiente = NULL;
	if(cabecera == NULL){
		cabecera = nuevo;
	}else{
		NodoGrupo* ultimo = cabecera;
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}
		ultimo->siguiente = nuevo;
	}
}

void imprimirGrupos(NodoGrupo* cabecera){
	NodoGrupo* aux = cabecera;
	int i = 1;
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"|#|\t|Codigo|\t|Materia|\t|Profesor|\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	while(aux != NULL){
		cout << "|" << i << ".|\t|" << aux->codigo << "|\t\t|" << aux->materia->nombre << "|\t|" << aux->profesor->nombre << "|" <<endl;
		cout<<"-----------------------------------------------------------------------------------------------------\n";
		aux = aux->siguiente;
		i++;
	}
	cout<< endl;
}

NodoGrupo* buscarGrupoId(NodoGrupo* cabecera, int codigo){
	if(cabecera == NULL){
		cout<<"No hay grupos registrados\n";
		return NULL;
	}else{
		NodoGrupo* aux = cabecera;
		while(aux != NULL){
			if(aux->codigo == codigo){
				return aux;
			}
			aux = aux->siguiente;
		}
	}
	cout<<"El grupo no existe\n";
	return NULL;
}

void cambiarProfesorDeGrupo(NodoGrupo* &grupo, NodoEstProf* profesor){
	grupo->profesor = profesor;
}

void agregarEstudianteAgrupo(NodoEstudianteGrupo* &cabecera, NodoEstProf* estudiante){
	NodoEstudianteGrupo* nuevo = new NodoEstudianteGrupo;
	nuevo->estudiante = estudiante;
	nuevo->siguiente = NULL;
	if(cabecera == NULL){
		cabecera = nuevo;
	}else{
		NodoEstudianteGrupo* ultimo = cabecera;
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}
		ultimo->siguiente = nuevo;
	}
}

void imprimirEstudiantesDeUnGrupo(NodoEstudianteGrupo* cabecera){
	NodoEstudianteGrupo* aux = cabecera;
	int i = 1;
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"|#|\t|Codigo|\t|Nombre|\t\t\t|Correo|\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	while(aux != NULL){
		cout<< "|" << i << ".|\t|" << aux->estudiante->codigo << "|\t|" << aux->estudiante->nombre << "|\t\t\t|" << aux->estudiante->correo << "|\n";
		cout<<"-----------------------------------------------------------------------------------------------------\n";
		aux = aux->siguiente;
		i++;
	}
	cout<<endl;
}

NodoEstudianteGrupo* buscarEstudianteGrupo(NodoEstudianteGrupo* cabecera, int codigo){
	if(cabecera == NULL){
		cout<<"No hay estudiantes registrados en este grupo.\n";
		return NULL;
	}else{
		NodoEstudianteGrupo* aux = cabecera;
		while(aux != NULL){
			if(aux->estudiante->codigo == codigo){
				return aux;
			}
			aux = aux->siguiente;
		}
	}
	cout<<"El Estudiante no pertenece a este grupo.\n";
	return NULL;
}

void eliminarEstudianteDeUnGrupo(NodoEstudianteGrupo* &cabecera, NodoEstudianteGrupo* &eliminar){
	if(cabecera == eliminar){
		cabecera = cabecera->siguiente;
	}else{
		NodoEstudianteGrupo* aux = cabecera;
		while(aux->siguiente != eliminar){
			aux = aux->siguiente;
		}
		aux->siguiente = eliminar->siguiente;
	}
	delete eliminar;
}

void menuGrupos(NodoGrupo* &cabecera, NodoEstProf* cabeceraProfesores, NodoEstProf* cabeceraEstudiantes, NodoMateria* cabeceraMateria){
	int menu = 0;
	int codigo;
	string codMateria;
	NodoGrupo* buscarGrupo = NULL;
	NodoEstProf* buscarEstProf = NULL;
	NodoMateria* buscarMateria = NULL;
	NodoEstudianteGrupo* buscarEstGrupo = NULL;
	while(menu != 7){
		cout<<"\t\t\tMENU GRUPOS\n";
		cout<<"1. Registrar grupo.\n";
		cout<<"2. Imprimir lista de grupos.\n";
		cout<<"3. Cambiar el profesor de un grupo.\n";
		cout<<"4. Agregar estudiante a un grupo.\n";
		cout<<"5. Ver los estudiantes de un grupo.\n";
		cout<<"6. Eliminar un estudiante de un grupo.\n";
		cout<<"7. Salir.\n";
		cin>>menu;
		switch(menu){
			case 1:
				if(cabeceraProfesores == NULL){
					cout<<"No hay profesores registrados.\n";
				}else{
					do{
						cout<<"Escriba el codigo del profesor del grupo: ";
						cin>>codigo;
						buscarEstProf = buscarEstProfCodigo(cabeceraProfesores, codigo);
						if(buscarEstProf == NULL){
							cout<<"No hay ningun profesor registado con ese codigo.\n";
						}
					}while(buscarEstProf == NULL);
				}
				do{
					cout<<"Escriba el codigo de la materia del grupo: ";
					cin>>codMateria;
					buscarMateria = buscarMateriaCodigo(cabeceraMateria, codMateria);
					if(buscarMateria == NULL){
						cout<<"No hay una materia registrada con ese codigo.\n";
					}
				}while(buscarMateria == NULL);
				registrarGrupo(cabecera, buscarMateria,buscarEstProf);
			break;
			case 2:
				imprimirGrupos(cabecera);
			break;
			case 3:
				imprimirGrupos(cabecera);
				do{
					cout<<"Escriba el codigo del grupo al que le quiere cambiar el profesor: ";
					cin>>codigo;
					buscarGrupo = buscarGrupoId(cabecera, codigo);
				}while(buscarGrupo == NULL);
				if(cabeceraProfesores == NULL){
					cout<<"No hay profesores registrados.\n";
				}else{
					imprimirListaEstProf(cabeceraProfesores);
					do{
						cout<<"Escriba el codigo del profesor del grupo: ";
						cin>>codigo;
						buscarEstProf = buscarEstProfCodigo(cabeceraProfesores, codigo);
						if(buscarEstProf == NULL){
							cout<<"No hay ningun profesor registado con ese codigo.\n";
						}
					}while(buscarEstProf == NULL);
				}
				cambiarProfesorDeGrupo(buscarGrupo, buscarEstProf);
			break;
			case 4:
					imprimirGrupos(cabecera);
				do{
					cout<<"Escriba el codigo del grupo al que le quiere agregar un estudiante: "; 
					cin>>codigo;
					buscarGrupo = buscarGrupoId(cabecera, codigo);
				}while(buscarGrupo == NULL);
				if(cabeceraEstudiantes == NULL){
					cout<<"No hay estudiantes registrados.\n";
				}else{
					imprimirListaEstProf(cabeceraEstudiantes);
					do{
						cout<<"Escriba el codigo del estudiante: ";
						cin>>codigo;
						buscarEstProf = buscarEstProfCodigo(cabeceraEstudiantes, codigo);
						if(buscarEstProf == NULL){
							cout<<"No hay ningun estudiante registado con ese codigo.\n";
						}
					}while(buscarEstProf == NULL);
				}
				agregarEstudianteAgrupo(buscarGrupo->cabeceraEstudiantes,buscarEstProf);
				
			break;
			case 5:
					imprimirGrupos(cabecera);
				do{
					cout<<"Escriba el codigo del grupo: "; 
					cin>>codigo;
					buscarGrupo = buscarGrupoId(cabecera, codigo);
				}while(buscarGrupo == NULL);
				imprimirEstudiantesDeUnGrupo(buscarGrupo->cabeceraEstudiantes);
			break;
			case 6:
				imprimirGrupos(cabecera);
				do{
					cout<<"Escriba el codigo del grupo al que le quiere eliminar un estudiante: "; 
					cin>>codigo;
					buscarGrupo = buscarGrupoId(cabecera, codigo);
				}while(buscarGrupo == NULL);
				if(cabeceraEstudiantes == NULL){
					cout<<"No hay estudiantes registrados.\n";
				}else{
					imprimirEstudiantesDeUnGrupo(buscarGrupo->cabeceraEstudiantes);
					do{
						cout<<"Escriba el codigo del estudiante que quiere eliminar del grupo: ";
						cin>>codigo;
						buscarEstGrupo = buscarEstudianteGrupo(buscarGrupo->cabeceraEstudiantes, codigo);
						if(buscarEstGrupo == NULL){
							cout<<"No hay ningun estudiante registado con ese codigo.\n";
						}
					}while(buscarEstGrupo == NULL);
				}
				eliminarEstudianteDeUnGrupo(buscarGrupo->cabeceraEstudiantes,buscarEstGrupo);
			break;
			case 7:
				cout<<"salida\n";
			break;
		}
	}
}




struct NodoAsistencia{
	NodoEstProf* estudiante;
	bool estado;
	NodoAsistencia* siguiente;
};

struct NodoClase{
	int codigo;
	NodoGrupo* grupo;
	string fecha;
	string horaInicio;
	string horaFin;
	NodoAsistencia* cabeceraAsistencia;
	NodoClase* siguiente;
};

int getCodigoClase(NodoClase* cabecera){
	NodoClase* ultimo = cabecera;
	if(cabecera == NULL){
		return 1; 
	}else{
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}	
	}
	return (ultimo->codigo)+1;
}


void registrarClase(NodoClase* &cabecera,NodoGrupo* grupo ,string fecha, string horaInicio, string horaFin){
	NodoClase* nuevo = new NodoClase;
	nuevo->codigo = getCodigoClase(cabecera);
	nuevo->grupo = grupo;
	nuevo->fecha = fecha;
	nuevo->horaInicio = horaInicio;
	nuevo->horaFin = horaFin;
	nuevo->cabeceraAsistencia = NULL;
	nuevo->siguiente = NULL;
	if(cabecera == NULL){
		cabecera = nuevo;
	}else{
		NodoClase* ultimo = cabecera;
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}
		ultimo->siguiente = nuevo;
	}
}

void imrpimirClases(NodoClase* cabecera){
	NodoClase* aux = cabecera;
	int i = 1;
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"|#|\t|Codigo|\t|Fecha|\t\t|Hora inicio|\t|Hora fin|\t|Grupo|\t\t|Materia|\t|Profesor|\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	while(aux != NULL){
		cout << "|" << i << "|\t|" << aux->codigo << "|\t\t|" <<aux->fecha << "|\t|" << aux->horaInicio << "|\t\t|" << aux->horaFin << "|\t\t|" << aux->grupo->codigo << "|\t\t|" << aux->grupo->materia->nombre << "|\t|" << aux->grupo->profesor->nombre << "|" << endl;
		cout<<"-----------------------------------------------------------------------------------------------------\n";
		aux = aux->siguiente;
		i++;
	}
	cout<<endl;
}

NodoClase* buscarClase(NodoClase* cabecera, int codigo){
	NodoClase* aux = cabecera;
	if(cabecera == NULL){
		cout<<"No hay clases registradas.\n";
		return NULL;
	}else{
		while(aux != NULL){
			if(aux->codigo == codigo){
				return aux;
			}
			aux = aux->siguiente;
		}
	}
	if (aux == NULL){
		cout<<"No se encuentra una clase con ese codigo.\n";
			return NULL;
	}
}

void imprimirClasesDeUnGrupo(NodoClase* cabecera, NodoGrupo* grupo){
	NodoClase* aux = cabecera;
	while(aux != NULL){
		if(aux->grupo == grupo){
			cout<<aux->codigo << ". " << aux->grupo->materia->nombre << " " << aux->grupo->profesor->nombre << " " << aux->fecha << " " << aux->horaInicio << " " << aux->horaFin << endl;
		}
		aux = aux->siguiente;
	}
	cout<<endl;
}

void registrarAsistencia(NodoAsistencia* &cabecera, NodoEstProf* estudiante, bool estado){
	NodoAsistencia* nuevo = new NodoAsistencia;
	nuevo->estudiante = estudiante;
	nuevo->estado = estado;
	nuevo->siguiente = NULL;
	if(cabecera == NULL){
		cabecera = nuevo;
	}else{
		NodoAsistencia* ultimo = cabecera;
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}
		ultimo->siguiente = nuevo;
	}
}

void llamarLista(NodoAsistencia* &cabecera, NodoEstudianteGrupo* cabEstudiantesDeUnGrupo){
	NodoEstudianteGrupo* aux = cabEstudiantesDeUnGrupo;
	int estado;
	bool est;
	while(aux != NULL){
		cout<<aux->estudiante->nombre<<endl;
		cout<<"Escriba 1 si el estudiante esta presente o 0 si el estudiante esta ausente: ";
		cin>>estado;
		switch(estado){
			case 0:
				est = false;
			break;
			case 1:
				est = true;
			break;
			default:
				est = true;
			break;
		}
		registrarAsistencia(cabecera, aux->estudiante, est);
		aux = aux->siguiente;
	}
}

void mostrarAsistenciaDeUnaClase(NodoAsistencia* cabecera){
	NodoAsistencia* aux = cabecera;
	string NombreA = "";
	int i = 1;
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"|#|\t|Nombre|\t\t\t\t|Asistencia|\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	while(aux != NULL){
		if(aux->estado == true){
			NombreA = "Presente";
		}else{
			NombreA = "Ausente";
		}
		cout<< "|" << i << "|\t|"<<aux->estudiante->nombre << "|\t\t\t\t|" << NombreA << "|\n" << endl;
		aux = aux->siguiente;
		i++;
	}
	cout<<endl;
}

void menuClases(NodoClase* &cabecera, NodoGrupo* cabeceraGrupos){
	int menu = 0;
	NodoGrupo* buscarGrupo = NULL;
	NodoClase* clas = NULL;
	int codigo;
	string fecha, horaInicio, horaFin;
	while(menu != 5){
		cout<<"\t\t\tMENU Clases\n";
		cout<<"1. Registrar clase.\n";
		cout<<"2. Imprimir lista de clases.\n";
		cout<<"3. Llamar asistencia de un clase.\n";
		cout<<"4. ver asistencia de una clase.\n";
		cout<<"5. Salir.\n";
		cin>>menu;
		switch(menu){
			case 1:
				do{
					cout<<"Escriba el codigo del grupo al que le quiere registrar una clase: ";
					cin>>codigo;
					buscarGrupo = buscarGrupoId(cabeceraGrupos, codigo);
				}while(buscarGrupo == NULL);
				cout<<"Escriba la fecha de la clase: ";
				cin>>fecha;
				cout<<"Escriba la hora de inicio de la clase: ";
				cin>>horaInicio;
				cout<<"Escriba la hora de finalizacion de la clase: ";
				cin>>horaInicio;
				registrarClase(cabecera, buscarGrupo, fecha, horaInicio, horaFin);
			break;
			case 2:
				imrpimirClases(cabecera);
			break;
			case 3:
				do{
					cout<<"Escriba el codigo del grupo que queire llamar lista: ";
					cin>>codigo;
					buscarGrupo = buscarGrupoId(cabeceraGrupos, codigo);
				}while(buscarGrupo == NULL);
				imprimirClasesDeUnGrupo(cabecera, buscarGrupo);
				do{
					cout<<"Escriba el codigo de la clase a la que le quiere registrar asistencia: ";
					cin>>codigo;
					clas = buscarClase(cabecera, codigo);
				}while(clas == NULL);
				llamarLista(clas->cabeceraAsistencia,buscarGrupo->cabeceraEstudiantes);
			break;
			case 4:
				do{
					cout<<"Escriba el codigo del grupo que queire ver la asistencia: ";
					cin>>codigo;
					buscarGrupo = buscarGrupoId(cabeceraGrupos, codigo);
				}while(buscarGrupo == NULL);
				imprimirClasesDeUnGrupo(cabecera, buscarGrupo);
				do{
					cout<<"Escriba el codigo de la clase a la que ver la asistencia: ";
					cin>>codigo;
					clas = buscarClase(cabecera, codigo);
				}while(clas == NULL);
				mostrarAsistenciaDeUnaClase(clas->cabeceraAsistencia);
			break;
			case 5:
				cout<<"SALIDA\n";
			break;
		}
	}
}

int main(){
	NodoEstProf* cabeceraEstudiantes= NULL;
	NodoEstProf* cabeceraProfesores= NULL;
	NodoMateria* cabeceraMaterias = NULL;
	NodoGrupo* cabeceraGrupos = NULL;
	NodoClase* cabeceraClases = NULL;
	int menu = 0;
	
	while(menu != 6){
		cout<<"\t\t\tMENU\n";
		cout<<"1. Menu estudiantes.\n";
		cout<<"2. Menu Profesores.\n";
		cout<<"3. Menu Materias.\n";
		cout<<"4. Menu Grupos.\n";
		cout<<"5. Menu Clases.\n";
		cout<<"6. Salir.\n";
		cout<<"\nSeleccione un menu:";
		cin>>menu;
		switch(menu){
			case 1:
				menuEstProf(cabeceraEstudiantes,"estudiantes", "estudiante");
			break;
			case 2:
				menuEstProf(cabeceraProfesores,"profesores", "profesor");
			break;
			case 3:
				menuMaterias(cabeceraMaterias);
			break;
			case 4:
				menuGrupos(cabeceraGrupos, cabeceraProfesores, cabeceraEstudiantes,cabeceraMaterias);
			break;
			case 5:
				menuClases(cabeceraClases, cabeceraGrupos);
			break;	
			case 6:
				cout<<"Salida";
			break;	
		}
	} 
	
	return 0;
}
